import json
from base64 import b64decode
from utils import rand_sni, validation

class Vmess:
    def __init__(self, config, name_tag):
        self.config = config
        self.name_tag = name_tag
        self._mentahan = None
        self._result = None
        self._vmess = None

    def _sanityzer_config(self):
        self.config = str(self.config).replace('vmess://', '')

    def _normalize_base64(self):
        self.config = self.config + ('=' * (-len(self.config) % 4))

    def _decode_config(self):
        try:
            self._mentahan = b64decode(self.config)
        except Exception as Err:
            print('-'*10)
            print("Error: {}".format(Err))
            print('-'*10)
            print("Base64 not valid...!!")
            print("")
            print("Normalizing....")
            self._normalize_base64()
            self._mentahan = b64decode(self.config)

        self._result = json.loads(self._mentahan)

    def _generate_account(self):
        settings = {
            "vnext": [{
                "address": self._result["add"],
                "port": int(self._result["port"]),
                "users": [
                    {
                        "alterId": int(self._result["aid"]),
                        "encryption": "auto",
                        "flow": "",
                        "id": self._result["id"],
                        "security": "auto"
                    }
                ]
            }]
        }
        streamSettings = {
            "network": self._result["net"],  # ? default `ws`
            "security": "tls",
            "tlsSettings": {
                "allowInsecure": True,
                "serverName": rand_sni()
            },
            "wsSettings": {
                "path": self._result["path"],
                "headers": {
                    "Host": self._result["host"]
                }
            },
            "sockopt": {
                "tcpFastOpen": True
            }
        }

        self._vmess = json.dumps({
            "mux": {
                "concurrency": 8,
                "enabled": False
            },
            "protocol": "vmess",
            "settings": settings,
            "streamSettings": streamSettings,
            "tag": self._result["add"] if len(self.name_tag) <= 0 else self.name_tag
        }, indent=2)

    def get_config(self):
        validation(r'^vmess://', self.config)
        print("Generate Account Vmess:")
        print('='*25)
        self._sanityzer_config()    # ? sanityzer config
        self._decode_config()       # ? decode base64 to object
        self._generate_account()    # ? generate account vmess

        return self._vmess
