import json
import re
from base64 import b64decode
from urllib.parse import unquote
from utils import rand_sni, validation

class Vless:
    def __init__(self, config, name_tag):
        self.config = unquote(config)
        self.name_tag = name_tag
        self._mentahan = None
        self._vless = None

    def __util_parse(self, regex, key_name):
        self.__dummy = re.findall(regex, self.config)
        self._mentahan[key_name] = self.__dummy[0] if len(self.__dummy) >= 1 else None
        self.__dummy = None

    def __reconstruct(self, key):
        self.__dummy = self._mentahan[key].split('&')
        self._mentahan[key] = self.__dummy[0] if len(self.__dummy) >= 1 else None
        self.__dummy = None

    def _parse_config(self):
        self._mentahan = {}
        self.__util_parse(r'\@(.*):',               'address')
        self.__util_parse(r'vless://.*:(.*)/\?',    'port')
        self.__util_parse(r'vless://(.*)\@',        'id')
        # ? must be reconstruct
        self.__util_parse(r'encryption\=(.*)&',    'encrypt')
        self.__util_parse(r'type\=(.*)&',          'type')
        self.__util_parse(r'security\=(.*)&',      'security')
        self.__util_parse(r'path\=(.*)&',          'path')
        self.__util_parse(r'\#(.*)',               'remark')

        self.__reconstruct('encrypt')
        self.__reconstruct('type')
        self.__reconstruct('security')
        self.__reconstruct('path')
        self.__reconstruct('remark')


    def _generate_account(self):
        settings = {
            "vnext": [{
                "address": self._mentahan['address'],
                "port": int(self._mentahan["port"]),
                "users": [{
                        "encryption": self._mentahan['encrypt'],
                        "id": self._mentahan["id"],
                        "security": "auto"
                }]
            }]
        }
        streamSettings = {
            "network": self._mentahan["type"],      # ! default: ws
            "security": self._mentahan["security"],     # ! default: tls
            "tlsSettings": {
                "allowInsecure": True,
                "serverName": rand_sni()
            },
            "wsSettings": {
                "path": self._mentahan["path"]
            },
            "sockopt": {
                "tcpFastOpen": True
            }
        }

        self._vless = json.dumps({
            "mux": {
                "concurrency": 8,
                "enabled": False
            },
            "protocol": "vless",
            "settings": settings,
            "streamSettings": streamSettings,
            "tag": self._mentahan["remark"] if len(self.name_tag) <= 0 else self.name_tag
        }, indent=2)

    def get_config(self):
        validation(r'^vless://', self.config)    # ? Validation
        print("Generate Account Vless:")
        print('='*25)
        self._parse_config()       # ? parse config
        self._generate_account()   # ? generate config
        self._mentahan = None

        return self._vless
