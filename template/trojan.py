
import json
import re
from urllib.parse import unquote
from utils import rand_sni, validation

class Trojan:
    def __init__(self, config, name_tag):
        self.config = unquote(config)
        self.name_tag = name_tag
        self._trojan = None
        self.__address = None
        self.__passwd = None

    def _parse_config(self):
        address = re.findall(r'\@(.*):', self.config)
        self.__address = address[0] if len(address) >= 1 else None

        passwd  = re.findall(r'trojan://(.*)\@', self.config)
        self.__passwd = passwd[0] if len(passwd) >= 1 else None

    def _generate_account(self):
        settings = {
            "servers": [{
                "address": self.__address,
                "level": 8,
                "method": "aes-128-cfb",
                "ota": False,
                "password": self.__passwd,
                "port": 443
            }]
        }
        streamSettings = {
            "network": "tcp",
            "security": "tls",
            "tlsSettings": {
                "allowInsecure": True,
                "serverName": rand_sni()
            },
            "sockopt": {
                "tcpFastOpen": True
            }
        }
        # ? Setup Account
        self._trojan = json.dumps({
            "mux": {
                "concurrency": 8,
                "enabled": False
            },
            "protocol": "trojan",
            "settings": settings,
            "streamSettings": streamSettings,
            "tag": "Proxy" if len(self.name_tag) <= 0 else self.name_tag
        }, indent=2)

    def get_config(self):
        validation(r'^trojan://', self.config)    # ? Validation
        print("Generate Account Trojan:")
        print('='*25)
        self._parse_config()       # ? parse config
        self._generate_account()   # ? generate config

        return self._trojan
