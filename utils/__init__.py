
import random
import re
import sys

def sanitiy_config(config, type_config):
    result = None

    if type_config == 'vmess':
        result = str(config).replace('vmess://', config)

    if type_config == 'trojan':
        result = str(config).replace('')

    return result

def rand_sni():
    sni = [
        # 'abtest-va-tiktok.byteoversea.com',
        'api2-t3.musical.ly',
        'p16-tiktokcdn-com.akamaized.net',
        'push-rtmp-f5.tiktokcdn.com.c.worldfcdn.com',
        'log.tiktokv.com'
        # 'muscdn.com',
        # 'musical.ly',
        # 'tiktok.com',
        # 'tiktokcdn.com',
        # 'tiktokv.com'
    ]
    return random.choice(sni)

def dbg(txt):
    print(f"[!] Debuger: {txt}")

def validation(target, config):
    if re.search(target, config) is None:
        print("Config not supported...")
        sys.exit()
