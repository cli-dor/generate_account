import re
import sys
from template import Vmess, Trojan, Vless


def notify(type_config):
    print("[*] Config {} Detected...".format(type_config))

def main():
    config   = input("Insert Config : ")
    tag_name = input("Name Tag      : ")
    print('============================')
    res = None

    if re.search(r'^trojan://', config):
        notify('trojan')
        res = Trojan(config, tag_name)
        print(res.get_config())
    elif re.search(r'^vmess://', config):
        notify('vmess')
        res = Vmess(config, tag_name)
        print(res.get_config())
    elif re.search(r'^vless://', config):
        notify('vless')
        res = Vless(config, tag_name)
        print(res.get_config())
    else:
        print("Config not supported...")
        sys.exit(1)


if __name__ == "__main__":
    main()
